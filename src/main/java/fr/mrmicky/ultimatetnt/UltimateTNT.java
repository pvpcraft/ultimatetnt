package fr.mrmicky.ultimatetnt;

import fr.mrmicky.ultimatetnt.utils.ConsoleOutput;
import fr.mrmicky.ultimatetnt.utils.FactionWatch;
import fr.mrmicky.ultimatetnt.utils.TntUtils;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.io.File;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public final class UltimateTNT extends JavaPlugin {

    public static final Random RANDOM = new Random();
    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("0.0");

    public static final Map<String, Integer> durabilityBlocks = new HashMap<>();
    private FactionWatch factionWatch;

    public ConsoleOutput cO;

    @Override
    public void onEnable() {
        saveDefaultConfig();

        cO = new ConsoleOutput(this);
        cO.setDebug(getConfig().getBoolean("Debug"));
        cO.setPrefix(ChatColor.translateAlternateColorCodes('&', getConfig().getString("Prefix")));

        loadOptions();

        factionWatch = new FactionWatch(this);

        getCommand("ultimatetnt").setExecutor(new CommandUltimateTNT(this));
        getServer().getPluginManager().registerEvents(new TNTListener(this), this);
        getServer().getPluginManager().registerEvents(factionWatch, this);
    }

    public void reload(CommandSender s) {
        long start = System.currentTimeMillis();

        File cfgFile = new File(getDataFolder(), "config.yml");

        if (!cfgFile.exists())
            saveResource("config.yml", false);

        reloadConfig();

        cO.setDebug(getConfig().getBoolean("Debug"));
        cO.setPrefix(ChatColor.translateAlternateColorCodes('&', getConfig().getString("Prefix")));

        cO.setReloadSender(s);

        loadOptions();

        factionWatch.reload();

        cO.setReloadSender(null);

        long stop = System.currentTimeMillis();

        s.sendMessage("§aDone.. reload took " + (stop - start) + "ms.");
    }

    public void loadOptions() {
        if (isDurabilityEnabled()) {
            ConfigurationSection section = getConfig().getConfigurationSection("BlockDurability.Blocks");

            for (String key : section.getKeys(false)) {
                try {
                    Material mat = Material.valueOf(key.toUpperCase());
                } catch (IllegalArgumentException e) {
                    cO.warn("Material " + key + " is not valid.");
                    continue;
                }

                // Add to tables
                cO.debug("Added " + key.toUpperCase() + " " + section.getInt(key));
                durabilityBlocks.put(key.toUpperCase(), section.getInt(key));
            }
        }

        cO.info("Loaded " + durabilityBlocks.size() + " durability block(s)..");
    }

    public FactionWatch getFactionWatch() {
        return factionWatch;
    }

    public boolean isWorldEnabled(World world) {
        return !containsIgnoreCase(getConfig().getStringList("DisableWorlds"), world.getName());
    }

    public String getRandomTNTName() {
        List<String> names = getConfig().getStringList("Names");
        return color(names.get(RANDOM.nextInt(names.size())));
    }

    public TNTPrimed spawnTNT(Location location, Entity source, String name) {
        return spawnTNT(location, source, name, true);
    }

    public int getMaxExplosions(Material material) {
        cO.debug("LF: " + material.name().toUpperCase());
        return durabilityBlocks.getOrDefault(material.name().toUpperCase(), 1);
    }

    public boolean isDurabilityEnabled() {
        return getConfig().getBoolean("BlockDurability.Enabled");
    }

    public boolean isDurabilityBlock(Material material) {
        return isDurabilityBlock(material.name());
    }

    public boolean isDurabilityBlock(String material) {
        return durabilityBlocks.containsKey(material.toUpperCase());
    }

    public TNTPrimed spawnTNT(Location location, Entity source, String name, boolean applyVelocity) {
        TNTPrimed tnt = location.getWorld().spawn(location.add(0.5, 0.25, 0.5), TNTPrimed.class);

        if (applyVelocity) {
            tnt.setVelocity(new Vector(0, 0.25, 0));
            tnt.teleport(location);
        }
        tnt.setIsIncendiary(getConfig().getBoolean("Fire"));
        tnt.setFuseTicks(getConfig().getInt("ExplodeTicks"));
        tnt.setYield((float) getConfig().getDouble("Radius"));

        if (getConfig().getBoolean("CustomName")) {
            tnt.setCustomNameVisible(true);

            if (!name.contains("%timer")) {
                tnt.setCustomName(name);
            } else {
                new BukkitRunnable() {

                    @Override
                    public void run() {
                        tnt.setCustomName(name.replace("%timer", DECIMAL_FORMAT.format(tnt.getFuseTicks() / 20.0)));

                        if (!tnt.isValid() || tnt.getFuseTicks() <= 0) {
                            cancel();
                        }
                    }
                }.runTaskTimer(this, 0, 1);
            }
        }

        if (source != null) {
            try {
                TntUtils.setTntSource(tnt, source);
            } catch (ReflectiveOperationException e) {
                getLogger().warning("Cannot set the source for " + tnt + ": " + e.getClass().getSimpleName() + " " + e.getMessage());
            }
        }
        return tnt;
    }

    public String color(String s) {
        return ChatColor.translateAlternateColorCodes('&', s);
    }

    public boolean containsIgnoreCase(List<String> list, String s) {
        return list.stream().anyMatch(s::equalsIgnoreCase);
    }
}
