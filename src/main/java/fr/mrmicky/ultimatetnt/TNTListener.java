package fr.mrmicky.ultimatetnt;

import com.massivecraft.factions.Faction;
import com.massivecraft.factions.Factions;
import fr.mrmicky.ultimatetnt.utils.BlockLocation;
import org.bukkit.*;
import org.bukkit.block.*;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDispenseEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Directional;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.*;

public class TNTListener implements Listener {

    private final Set<UUID> throwCooldown = new HashSet<>();

    private final Map<UUID, BlockLocation> fallingBlocks = new HashMap<>();
    private final Map<BlockLocation, BlockLocation> blocks = new HashMap<>();
    private final List<List<Block>> safeBlocks = new ArrayList<>();

    // Block locations that have are damaged and should have different durability
    private final Map<BlockLocation, Integer> blockDurability = new HashMap<>();

    private final UltimateTNT plugin;

    public TNTListener(UltimateTNT plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent e) {
        // Remove even if it isn't a durability block, we don't want useless blocks stacking in the map
        blockDurability.remove(new BlockLocation(e.getBlock()));
    }

    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onBlockPlace(BlockPlaceEvent e) {
        Player player = e.getPlayer();
        Block block = e.getBlock();

        if (block.getType() != Material.TNT) {
            return;
        }

        if (!plugin.getConfig().getBoolean("AutoIgnite") || !plugin.isWorldEnabled(block.getWorld())) {
            return;
        }

        block.setType(Material.AIR);
        plugin.spawnTNT(block.getLocation(), player, plugin.getRandomTNTName());
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onChunkUnload(ChunkUnloadEvent e) {
        blockDurability.keySet().removeIf(loc -> loc.isInChunk(e.getChunk()));
    }

    @SuppressWarnings("deprecation")
    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerInteract(PlayerInteractEvent e) {
        Player player = e.getPlayer();
        ItemStack item = e.getItem();
        Block block = e.getClickedBlock();

        if (item == null || !plugin.isWorldEnabled(player.getWorld())) {
            return;
        }

        if (plugin.getConfig().getBoolean("Throw.Enable") && player.getItemInHand().getType() == Material.TNT) {
            if (!(player.isSneaking() && plugin.getConfig().getBoolean("Throw.DisableOnSneak"))
                    && (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK)) {
                e.setCancelled(true);

                if (throwCooldown.contains(player.getUniqueId())) {
                    Bukkit.getScheduler().runTask(plugin, player::updateInventory);
                    return;
                }

                Vector velocity = player.getLocation().getDirection().multiply(plugin.getConfig().getDouble("Throw.Velocity"));
                plugin.spawnTNT(player.getEyeLocation(), player, plugin.getRandomTNTName(), false).setVelocity(velocity);
                throwCooldown.add(player.getUniqueId());
                Bukkit.getScheduler().runTaskLater(plugin,
                        () -> throwCooldown.remove(player.getUniqueId()), plugin.getConfig().getInt("Throw.Delay") * 20);

                if (player.getGameMode() != GameMode.CREATIVE) {
                    ItemStack handItem = player.getItemInHand();
                    if (handItem.getAmount() > 1) {
                        handItem.setAmount(handItem.getAmount() - 1);
                    } else {
                        handItem = null;
                    }

                    player.setItemInHand(handItem);
                }

                Sound sound = Sound.valueOf(Bukkit.getVersion().contains("1.8") ? "CHICKEN_EGG_POP" : "ENTITY_CHICKEN_EGG");
                player.playSound(player.getLocation(), sound, 1.0F, 1.0F);
            }
        } else if (e.getAction() == Action.RIGHT_CLICK_BLOCK && !e.isCancelled() && block != null && block.getType() == Material.TNT) {
            String typeName = item.getType().name(); // 1.13 support
            if (item.getType() != Material.FLINT_AND_STEEL && !typeName.equals("FIREBALL") && !typeName.equals("FIRE_CHARGE")) {
                return;
            }

            block.setType(Material.AIR);
            plugin.spawnTNT(block.getLocation(), player, plugin.getRandomTNTName());
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onBlockDispense(BlockDispenseEvent e) {
        ItemStack item = e.getItem();

        if (item.getType() != Material.TNT || e.getBlock().getType() != Material.DISPENSER) {
            return;
        }

        Dispenser dispenser = (Dispenser) e.getBlock().getState();
        Inventory inv = dispenser.getInventory();
        BlockFace face = ((Directional) dispenser.getData()).getFacing();
        Block block = e.getBlock().getRelative(face);

        e.setCancelled(true);

        plugin.spawnTNT(block.getLocation(), null, plugin.getRandomTNTName());

        Bukkit.getScheduler().runTask(plugin, () -> {
            int slot = inv.first(Material.TNT);

            if (slot < 0) {
                plugin.getLogger().warning("No TNT in BlockDispenseEvent");
                return;
            }

            ItemStack tntItem = inv.getItem(slot);
            if (tntItem.getAmount() > 1) {
                tntItem.setAmount(tntItem.getAmount() - 1);
                inv.setItem(slot, tntItem);
            } else {
                inv.clear(slot);
            }
        });
    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent e) {
        if (e.getEntityType() != EntityType.PLAYER || e.getCause() != DamageCause.FALL) {
            return;
        }

        if (plugin.isWorldEnabled(e.getEntity().getWorld())) {
            e.setDamage(e.getDamage() / plugin.getConfig().getDouble("FallDamage"));
        }
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
        if (e.getEntityType() != EntityType.PLAYER || e.getDamager().getType() != EntityType.PRIMED_TNT) {
            return;
        }

        if (plugin.isWorldEnabled(e.getEntity().getWorld())) {
            e.setDamage(e.getDamage() / plugin.getConfig().getDouble("TNTDamage"));
        }
    }

    /*@EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onEntityExplodeLowest(EntityExplodeEvent e) {

        plugin.cO.debug("LOWEST");

        if (e.getEntity().getType().equals(EntityType.PRIMED_TNT)) {
            TNTPrimed tnt = (TNTPrimed) e.getEntity();

            plugin.cO.debug("MAT: " + tnt.getLocation().getBlock().getType().name());

            if ((tnt.getLocation().getBlock().getType().equals(Material.WATER)
                    || tnt.getLocation().getBlock().getType().equals(Material.STATIONARY_WATER))
                    && plugin.getConfig().getBoolean("DestroyBlocksWhileInWater")) {

               // Before

                List<Block> before = new ArrayList<>();
                List<Block> changedBlocks = new ArrayList<>();

                int radius = 4;

                Block center = tnt.getLocation().getBlock();

                // For every block in radius
                for (int x = -radius; x <= radius; x++) {
                    for (int y = -radius; y <= radius; y++) {
                        for (int z = -radius; z <= radius; z++) {

                            // Get relative block
                            Block relativeBlock = center.getRelative(x, y, z);

                            if (relativeBlock.getType().equals(Material.AIR))
                                continue;

                            before.add(relativeBlock);
                        }
                    }
                }

                plugin.cO.debug("bef: " + before.size());

                tnt.getLocation().getBlock().setType(Material.AIR);

                tnt.getLocation().getWorld().createExplosion(tnt.getLocation().getX(), tnt.getLocation().getY(), tnt.getLocation().getZ(),
                        (int) plugin.getConfig().getDouble("WaterRadius"), plugin.getConfig().getBoolean("Fire"), true);

                new BukkitRunnable() {
                    @Override
                    public void run() {
                        tnt.getLocation().getBlock().getState().update(true);
                    }
                }.runTaskLaterAsynchronously(plugin, 5);

                // After

                new BukkitRunnable() {
                    @Override
                    public void run() {
                        // For every block in radius
                        for (int x = -radius; x <= radius; x++) {
                            for (int y = -radius; y <= radius; y++) {
                                for (int z = -radius; z <= radius; z++) {

                                    // Get relative block
                                    Block relativeBlock = center.getRelative(x, y, z);

                                    for (Block b : before) {
                                        if (b.getLocation().equals(relativeBlock.getLocation())) {
                                            if (!b.getType().equals(relativeBlock.getType()))
                                                changedBlocks.add(relativeBlock);
                                        }
                                    }
                                }
                            }
                        }

                        plugin.cO.debug("CHB: " + changedBlocks.size());
                    }
                }.runTaskLater(plugin, 100);

                e.blockList().addAll(changedBlocks);

                plugin.cO.debug("BL: " + e.blockList().size());
            }
        }
    }*/

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void onEntityExplodeLow(EntityExplodeEvent e) {

        // Only look for durability actions here
        if (e.getEntityType() != EntityType.PRIMED_TNT || !plugin.isDurabilityEnabled()) {
            return;
        }

        if (!plugin.isWorldEnabled(e.getEntity().getWorld()))
            return;

        TNTPrimed tnt = (TNTPrimed) e.getEntity();

        int radius = (int) tnt.getYield();

        Block block = e.getLocation().getBlock();

        // For every block in radius
        for (int x = -radius; x <= radius; x++) {
            for (int y = -radius; y <= radius; y++) {
                for (int z = -radius; z <= radius; z++) {

                    // Get relative block
                    Block relativeBlock = block.getRelative(x, y, z);

                    boolean boostDurability = false;

                    if (Factions.getInstance().isClaimed(relativeBlock.getLocation())) {
                        Faction faction = Factions.getInstance().getByLocation(relativeBlock.getLocation());

                        boostDurability = plugin.getFactionWatch().boostDurability(faction);
                    }

                    // Check only for obsidian
                    if (relativeBlock.getType().equals(Material.AIR) || relativeBlock.getType().equals(Material.BEDROCK) || (!plugin.isDurabilityBlock(relativeBlock.getType()) && !boostDurability))
                        continue;

                    BlockLocation blockLocation = new BlockLocation(relativeBlock);

                    boolean inWater = inWater(relativeBlock);

                    plugin.cO.debug("BD: " + boostDurability);
                    plugin.cO.debug("inWater: " + inWater);

                    // Get max number of explosions it can take

                    int maxAmount = plugin.getMaxExplosions(relativeBlock.getType());

                    if (inWater) {
                        // Water durability boost
                        maxAmount *= plugin.getConfig().getInt("WaterMultiplier");
                    } else if (boostDurability) {
                        // Faction offline durability boost
                        maxAmount *= plugin.getConfig().getInt("OfflineMultiplier");
                    }

                    plugin.cO.debug("MaxA: " + maxAmount);

                    // Number of explosions
                    int explosions = blockDurability.getOrDefault(blockLocation, 0) + 1;

                    plugin.cO.debug("Explosions: " + explosions);

                    if (explosions < maxAmount) {
                        // If lower, update
                        blockDurability.put(blockLocation, explosions);

                        // Remove, do not destroy.
                        e.blockList().remove(relativeBlock);
                    } else {
                        // if higher, destroy the block already
                        blockDurability.remove(blockLocation);

                        e.blockList().add(relativeBlock);
                    }
                }
            }
        }
    }

    private boolean inWater(Block block) {
        for (BlockFace face : BlockFace.values()) {
            Block fBlock = block.getRelative(face);

            if (fBlock.getType().equals(Material.WATER) || fBlock.getType().equals(Material.STATIONARY_WATER))
                return true;
        }

        return false;
    }

    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onEntityExplode(EntityExplodeEvent e) {

        if (e.getEntityType() != EntityType.PRIMED_TNT && e.getEntityType() != EntityType.MINECART_TNT && !plugin.getConfig().getBoolean("AllExplosions")) {
            return;
        }

        Entity entity = e.getEntity();

        // Check if world is enabled
        if (!plugin.isWorldEnabled(entity.getWorld())) {
            return;
        }

        // Get TNT source
        Entity tnt = e.getEntityType() == EntityType.PRIMED_TNT ? ((TNTPrimed) entity).getSource() : null;

        // Load Configuration options
        boolean noBreak = plugin.getConfig().getBoolean("DisableBreak");
        boolean realistic = plugin.getConfig().getBoolean("RealisticExplosion");
        boolean restore = plugin.getConfig().getBoolean("RestoreBlocks.Enable");
        boolean whitelist = plugin.getConfig().getBoolean("Whitelist.Enable");

        List<String> blockBlacklist = plugin.getConfig().getStringList("BlacklistBlocks");
        List<String> blockWhitelist = plugin.getConfig().getStringList("Whitelist.BlockList");

        // TNT name?
        String name = plugin.getRandomTNTName();

        int maxFallingBlocks = plugin.getConfig().getInt("MaxFallingBlocksPerChunk") - getFallingBlocksInChunk(e.getLocation().getChunk());

        if (plugin.getConfig().getBoolean("DisableDrops")) {
            e.setYield(0.0F);
        }

        // All blocks affected by the explosion
        Iterator<Block> blockIterator = e.blockList().iterator();

        while (blockIterator.hasNext()) {
            Block block = blockIterator.next();

            // If it's a tnt
            if (block.getType() == Material.TNT) {

                // Remove it?
                block.setType(Material.AIR);

                // Spawn a new one, aha.
                plugin.spawnTNT(block.getLocation(), tnt, name);

                // Remove it from iterator
                blockIterator.remove();
            } else if (noBreak || plugin.containsIgnoreCase(blockBlacklist, block.getType().toString())
                    || (whitelist && !plugin.containsIgnoreCase(blockWhitelist, block.getType().toString()))) {

                // If we don't want it to be affected, remove from block iterator.
                blockIterator.remove();
            } else if (realistic) {
                if (!blocks.containsValue(new BlockLocation(block)) && isNotSafeBlock(block)) {

                    if (maxFallingBlocks > 0) {

                        double x = (Math.random() - Math.random()) / 1.5;
                        double y = Math.random();
                        double z = (Math.random() - Math.random()) / 1.5;

                        // noinspection deprecation
                        FallingBlock fall = block.getWorld().spawnFallingBlock(block.getLocation(), block.getType(), block.getData());

                        fall.setDropItem(false);
                        fall.setVelocity(new Vector(x, y, z));
                        maxFallingBlocks--;

                        if (restore) {
                            restoreBlock(block, fall);
                        }
                    } else if (restore) {
                        restoreBlock(block, null);
                    }

                    block.setType(Material.AIR);
                }
            } else if (restore && block.getType() != Material.AIR) {
                restoreBlock(block, null);
            }

            if (!realistic && block.getType() == Material.OBSIDIAN) {
                if (e.getYield() > 0) {
                    block.breakNaturally();
                } else {
                    block.setType(Material.AIR);
                }
            }
        }

        if (realistic && restore) {
            List<Block> blocks = new ArrayList<>(e.blockList());
            safeBlocks.add(blocks);
            Bukkit.getScheduler().runTaskLater(plugin, () ->
                    safeBlocks.remove(blocks), plugin.getConfig().getInt("RestoreBlocks.MaxDelay") + 60);
        }
    }

    @EventHandler
    public void onEntityChangeBlock(EntityChangeBlockEvent e) {
        if (!(e.getEntity() instanceof FallingBlock)) {
            return;
        }

        BlockLocation location = fallingBlocks.remove(e.getEntity().getUniqueId());
        if (location != null) {
            blocks.put(location, new BlockLocation(e.getBlock()));
        }
    }

    private void restoreBlock(Block block, FallingBlock fallingBlock) {

        int min = plugin.getConfig().getInt("RestoreBlocks.MinDelay");
        int max = plugin.getConfig().getInt("RestoreBlocks.MaxDelay");

        if (fallingBlock != null) {
            fallingBlocks.put(fallingBlock.getUniqueId(), new BlockLocation(block));
        }

        BlockState state = block.getState();
        String[] signLines = state instanceof Sign ? ((Sign) state).getLines() : null;
        ItemStack[] items = state instanceof InventoryHolder ? ((InventoryHolder) state).getInventory().getContents() : null;

        if (items != null) {
            for (int i = 0; i < items.length; i++) {
                if (items[i] != null) {
                    items[i] = items[i].clone();
                }
            }
        }

        Bukkit.getScheduler().runTaskLater(plugin, () -> {
            if (fallingBlock != null) {
                if (fallingBlock.isValid()) {
                    fallingBlock.remove();
                }
                fallingBlocks.remove(fallingBlock.getUniqueId());

                // Remove blocks spawn by falling blocks
                BlockLocation location = blocks.remove(new BlockLocation(block.getLocation()));
                if (location != null) {
                    Block oldBlock = block.getWorld().getBlockAt(location.getX(), location.getY(), location.getZ());
                    if (isNotSafeBlock(oldBlock)) {
                        oldBlock.setType(Material.AIR);
                    }
                }
            }

            if (!plugin.containsIgnoreCase(plugin.getConfig().getStringList("RestoreBlocks.RestoreBlacklist"), state.getType().toString())) {
                state.update(true);
                BlockState newState = block.getState();
                // restore inventory contents & signs lines
                if (signLines != null && newState instanceof Sign) {
                    Sign sign = (Sign) newState;
                    for (int i = 0; i < 4; i++) {
                        sign.setLine(i, signLines[i]);
                    }
                    sign.update();
                } else if (items != null && newState instanceof InventoryHolder) {
                    ((InventoryHolder) newState).getInventory().setContents(items);
                }
            }
        }, min + UltimateTNT.RANDOM.nextInt(max - min));
    }

    private int getFallingBlocksInChunk(Chunk chunk) {
        return (int) Arrays.stream(chunk.getEntities())
                .filter(e -> e.getType() == EntityType.FALLING_BLOCK)
                .count();
    }

    private boolean isNotSafeBlock(Block b) {
        return safeBlocks.stream().noneMatch(list -> list.contains(b));
    }
}
