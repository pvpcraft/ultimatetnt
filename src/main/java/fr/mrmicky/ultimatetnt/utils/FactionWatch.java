package fr.mrmicky.ultimatetnt.utils;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;
import fr.mrmicky.ultimatetnt.UltimateTNT;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;
import java.util.Map;

public class FactionWatch implements Listener {

    private final UltimateTNT plugin;

    // Faction ID, when to end durability boost
    private final Map<String, Long> timers = new HashMap<>();

    public FactionWatch(UltimateTNT plugin) {
        this.plugin = plugin;
    }

    // Is the whole faction offline?
    public boolean isOffline(Faction faction, FPlayer player) {
        for (FPlayer fPlayer : faction.getFPlayers()) {
            if (fPlayer.isOnline() && !fPlayer.equals(player))
                return false;
        }

        return true;
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {

        // Find out if he's the last one to leave

        FPlayer fPlayer = FPlayers.getInstance().getByPlayer(e.getPlayer());

        if (fPlayer.hasFaction()) {
            Faction faction = fPlayer.getFaction();

            if (isOffline(faction, fPlayer)) {
                // Set faction timer to current system time + cooldown from config in millis
                timers.put(faction.getId(), System.currentTimeMillis() + (plugin.getConfig().getLong("FactionOfflineCooldown") * 1000));
                plugin.cO.debug("Timers: " + timers.keySet().toString());
            }
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {

        // Check if one of the factions with an active timer hasn't rejoined

        FPlayer fPlayer = FPlayers.getInstance().getByPlayer(e.getPlayer());

        if (fPlayer.hasFaction()) {
            // Remove from the list
            timers.remove(fPlayer.getFactionId());
            plugin.cO.debug("Timers: " + timers.keySet().toString());
        }
    }

    // Should we boost durability of the block?

    public boolean boostDurability(Faction faction) {

        // If it has a timer
        if (timers.containsKey(faction.getId())) {
            if (timers.get(faction.getId()) <= System.currentTimeMillis()) {
                timers.remove(faction.getId());
                plugin.cO.debug("Timers: " + timers.keySet().toString());
                return false;
            } else return true;
        }

        return false;
    }

    public void reload() {
        timers.clear();
    }
}
